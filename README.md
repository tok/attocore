# Attocore - RV32E implementation in SpinalHDL

This is the construction place of an ottocore candidate written in SpinalHDL.

## The ottocore challenge

Ottocore candidates must implement RV32E with as little area as possible. No memory macros nor latches are allowed, hence the register file is made of flip-flops.

The project page of ottocore is not yet public. Write an issue if you are interested in more details.
See (not public yet): https://codeberg.org/ottocore/ottocore-framework

## Compiling to Verilog
To compile Attocore to a Verilog file as defined by the ottocore challenge run:
```bash
sbt run
# Then choose `ottocore.OttocoreVerilog`.
```

## Getting started with development
The easiest way to dive into the implementation of Attocore is by opening the project in a IDE for Scala such as IntellJ Idea with the Scala Plugin.

The top level component resides in `src/main/scala/ottocore/Attocore.scala`. This file also contains a `main` function that will compile the Attocore to Verilog.
The `tests` directory contains unit-tests which check that the RV32E implementation is not totally insane.

### Dependencies (Debian 11)

```
apt install openjdk-11-jdk-headless verilator
```

## Formal verification
Formal verification is not far yet but some first steps are there already.
Formal assertions are inline in the code of the core.
To generate a SystemVerilog file containing the assertions run `ottocore.AttocoreFormalSystemVerilog`.
The scripts in the `formal` folder can be used to run the verification.

```
                      ..::::::::::..                      
                   .::              :::.                  
                .::                    ::.                
              .::                        ::.              
             ::                            ::             
            ::                              ::            
           ::                                ::           
          .:                                  ::          
          ::                                  ::          
          ::                                   :          
          ::                                  ::          
          ::          :::.      .:::.         ::          
           ::        :   ::    .    :.       ::           
            :       : ...::    :....::       ::           
            ::      :::::::    :::::::      ::            
  .:::::::   :       ::::::     ::::::      :   :::::::.  
 ::   ...::: ::       :::        :::       :: :::::.   :: 
.:   ::       :            .::.            ::       ::  ::
::  ::        ::         : .... :         .:        ::  ::
::   :.      .::         :    :.          :::      .:   ::
 ::   ::.  .::::         : ::.: :         ::::.. .::   .: 
  ::    :::    :          :....:          ::    :     .:  
   ::          :                          :.         ::   
     ::.      ::                          ::      .::     
        :::.. ::                          :: ..:::        
::::.       :::         ::::::::::.        :::       ..:::
  :::::::::::        .::          ::.        :::::::::::  
    :::            .::              ::.            :::    
       :::.     .:::                   ::.     ..::       
           ::::::                         ::::::          


```
