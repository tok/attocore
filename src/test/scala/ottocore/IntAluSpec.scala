package ottocore

import org.scalatest.FlatSpec
import ottocore.plugins.{AluCtrl, IntAlu}
import spinal.core.SpinalEnumCraft
import spinal.core.sim._
import spinal.core._

import scala.util.Random

class IntAluSpec extends FlatSpec {

  // Test ALU with smaller bit width.
  val XLEN = 4

  val bitmask = (BigInt(1) << XLEN) - 1

  /**
   * Generate random BigInt with XLEN bits.
   * @return
   */
  def nextUInt(): BigInt = {
    BigInt(XLEN, Random)
  }


  val compiled = SimConfig.compile(new IntAlu(XLEN bits))

  val numIterations = 1000

  "The ALU" should "compute correct arithmetic and bitwise values." in {

    compiled.doSim { dut =>
      Random.setSeed(42)

      val operators = List(
        (AluCtrl.ADD, (a: BigInt, b: BigInt) => (a + b) & bitmask),
        (AluCtrl.SUB, (a: BigInt, b: BigInt) => (a - b) & bitmask),
        (AluCtrl.XOR, (a: BigInt, b: BigInt) => a ^ b),
        (AluCtrl.OR, (a: BigInt, b: BigInt) => a | b),
        (AluCtrl.AND, (a: BigInt, b: BigInt) => a & b)
      )

      for ((aluCtrl, function) <- operators) {
        for (i <- 0 until numIterations) {
          val a = nextUInt()
          val b = nextUInt()

          dut.io.cmd.valid #= true
          dut.io.rsp.ready #= false
          dut.io.cmd.operand_a #= a
          dut.io.cmd.operand_b #= b
          dut.io.cmd.control #= aluCtrl

          sleep(1)

          dut.io.cmd.valid #= false

          val expected = function(a, b)
          val actual = dut.io.rsp.result.toBigInt
          assertResult(expected)(actual)
        }
      }
    }
  }

  it should "compute correct 'Set Less Than (Unsigned)'" in {

    compiled.doSim { dut =>
      Random.setSeed(42)

      for (i <- 0 until numIterations) {
        val a = nextUInt()
        val b = nextUInt()

        dut.io.cmd.valid #= true
        dut.io.rsp.ready #= false
        dut.io.cmd.operand_a #= a
        dut.io.cmd.operand_b #= b
        dut.io.cmd.control #= AluCtrl.SetLessThanU

        sleep(1)

        dut.io.cmd.valid #= false

        val expected = if (a < b) 1 else 0
        val actual = dut.io.rsp.result.toBigInt
        assertResult(expected)(actual)
      }
    }
  }

  /**
   * Convert a BigInt in two's complement to signed form.
   *
   * @param a
   * @param numBits
   * @return
   */
  def fromTwoComplement(a: BigInt, numBits: Int): BigInt = {
    val msb = a.testBit(numBits - 1)
    if (msb) {
      val mask = (BigInt(1) << numBits) - 1
      -(((a ^ mask) + 1) & mask)
    } else {
      a
    }
  }

  it should "compute correct 'Set Less Than (Signed)'" in {

    compiled.doSim { dut =>
      Random.setSeed(42)

      for (i <- 0 until numIterations) {
        val a = nextUInt()
        val b = nextUInt()

        dut.io.cmd.valid #= true
        dut.io.rsp.ready #= false
        dut.io.cmd.operand_a #= a
        dut.io.cmd.operand_b #= b
        dut.io.cmd.control #= AluCtrl.SetLessThan

        sleep(1)

        dut.io.cmd.valid #= false

        val expected = if (fromTwoComplement(a, XLEN) < fromTwoComplement(b, XLEN)) 1 else 0
        val actual = dut.io.rsp.result.toBigInt
        assertResult(expected)(actual)
      }
    }
  }
}
