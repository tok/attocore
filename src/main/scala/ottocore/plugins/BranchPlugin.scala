package ottocore.plugins

import ottocore._
import spinal.core.Formal._
import spinal.core.GenerationFlags.formal
import spinal.core._
import spinal.lib._
import spinal.lib.fsm._

object BranchUnitCtrl extends SpinalEnum {
  val NO_BRANCH, BEQ, BNE, BLT, BGE, BLTU, BGEU = newElement()
}

case class BranchUnitCmd(XLEN: BitCount) extends Bundle {
  val aluRsp = AluRsp()
  val control = BranchUnitCtrl()
}

case class BranchUnitRsp(XLEN: BitCount) extends Bundle {
  val branchTaken = Bool()
  val branchAddr = UInt(XLEN)
}


class TwoCycleBranchUnit(XLEN: BitCount = 32 bits) extends Component {

  val io = new Bundle {
    val cmd = slave Stream BranchUnitCmd(XLEN)
    val rsp = master Stream BranchUnitRsp(XLEN)

    val aluCtrl = out(AluCtrl())
    val aluSrcA = out(AluSrcA())
    val aluSrcB = out(AluSrcB())
  }

  // Need SetLessThan to compute branch condition (comparison),
  // except for BLTU and BGEU where SetLessThanU is needed.
  io.aluCtrl := AluCtrl.SetLessThan
  io.aluSrcA := AluSrcA.RS1
  io.aluSrcB := AluSrcB.RS2

  // Wire the branch address to the result of the ALU.
  // This is only valid if a branch is taken.
  io.rsp.branchAddr := io.cmd.aluRsp.result.asUInt
  io.rsp.branchTaken := False

  // Compute branch condition.
  val branchTaken = Bool()
  switch(io.cmd.control) {
    import BranchUnitCtrl._
    val aEqualsB = io.cmd.aluRsp.aEqualsB
    val aLessThanB = io.cmd.aluRsp.aLessThanB
    is(NO_BRANCH) {
      branchTaken := False
    }
    is(BEQ) {
      branchTaken := aEqualsB
    }
    is(BNE) {
      branchTaken := !aEqualsB
    }
    is(BLT) {
      branchTaken := aLessThanB
    }
    is(BLTU) {
      io.aluCtrl := AluCtrl.SetLessThanU // Tell ALU to compute unsigned comparison.
      branchTaken := aLessThanB
    }
    is(BGE) {
      branchTaken := !aLessThanB | aEqualsB
    }
    is(BGEU) {
      io.aluCtrl := AluCtrl.SetLessThanU // Tell ALU to compute unsigned comparison.
      branchTaken := !aLessThanB | aEqualsB
    }
  }

  io.cmd.ready := False
  io.rsp.valid := False

  // TODO: more efficient FSM. Branch-not-taken should take only one cycle.

  val fsm = new StateMachine {
    val stateEntry: State = new State with EntryPoint {
      whenIsActive {
        io.cmd.ready := True
        when(io.cmd.fire) {
          when(branchTaken) {
            goto(stateBranchTaken)
          } otherwise {
            //            io.rsp.valid := True
            //            io.rsp.branchTaken := False
            goto(stateBranchNotTaken)
          }
        }
      }
    }
    val stateBranchNotTaken = new State {
      // Compute branch address.
      whenIsActive {
        io.rsp.valid := True
        io.rsp.branchTaken := False

        when(io.rsp.fire) {
          goto(stateEntry)
        }
      }
    }
    val stateBranchTaken = new State {
      // Compute branch address.
      whenIsActive {
        io.aluCtrl := AluCtrl.ADD
        io.aluSrcA := AluSrcA.PC
        io.aluSrcB := AluSrcB.IMM_B

        io.rsp.valid := True
        io.rsp.branchTaken := True

        when(io.rsp.fire) {
          goto(stateEntry)
        }
      }
    }
  }

}


class SingleCycleBranchUnit(XLEN: BitCount = 32 bits) extends Component {

  val io = new Bundle {
    val cmd = slave Stream BranchUnitCmd(XLEN)
    val rsp = master Stream BranchUnitRsp(XLEN)

    val aluCtrl = out(AluCtrl())
    val aluSrcA = out(AluSrcA())
    val aluSrcB = out(AluSrcB())

    val pc = in UInt  (XLEN)
    val imm_b = in UInt  (XLEN)
  }

  // Need SetLessThan to compute branch condition (comparison),
  // except for BLTU and BGEU where SetLessThanU is needed.
  io.aluCtrl := AluCtrl.SetLessThan
  io.aluSrcA := AluSrcA.RS1
  io.aluSrcB := AluSrcB.RS2


  // Compute branch condition.
  val branchTaken = Bool()
  switch(io.cmd.control) {
    import BranchUnitCtrl._
    val aEqualsB = io.cmd.aluRsp.aEqualsB
    val aLessThanB = io.cmd.aluRsp.aLessThanB
    is(NO_BRANCH) {
      branchTaken := False
    }
    is(BEQ) {
      branchTaken := aEqualsB
    }
    is(BNE) {
      branchTaken := !aEqualsB
    }
    is(BLT) {
      branchTaken := aLessThanB
    }
    is(BLTU) {
      io.aluCtrl := AluCtrl.SetLessThanU // Tell ALU to compute unsigned comparison.
      branchTaken := aLessThanB
    }
    is(BGE) {
      branchTaken := !aLessThanB | aEqualsB
    }
    is(BGEU) {
      io.aluCtrl := AluCtrl.SetLessThanU // Tell ALU to compute unsigned comparison.
      branchTaken := !aLessThanB | aEqualsB
    }
  }
  
  io.rsp.branchTaken := branchTaken

  // Separate adder for calculating the target address.
  io.rsp.branchAddr := io.pc + io.imm_b

  io.rsp.valid := io.cmd.valid
  io.cmd.ready := io.rsp.ready

}

class TwoCycleBranchPlugin(withFormalChecks: Boolean = true) extends Plugin {

  var branchUnit: TwoCycleBranchUnit = null

  override def setup(core: Attocore): Unit = {
    branchUnit = new TwoCycleBranchUnit()
  }

  override def build(core: Attocore): Unit = {

    branchUnit.io.cmd.valid := False
    branchUnit.io.rsp.ready := False
    branchUnit.io.cmd.aluRsp.aEqualsB := core.aluResultEquals
    branchUnit.io.cmd.aluRsp.aLessThanB := core.aluResultLess
    branchUnit.io.cmd.aluRsp.result := core.aluResult
    branchUnit.io.cmd.control := BranchUnitCtrl.NO_BRANCH

    val aluPlugins = core.findPlugins(classOf[IntAluPlugin])
    assert(aluPlugins.length == 1, s"Expected one ALU plugin. Found ${aluPlugins.length}")
    val aluPlugin = aluPlugins(0)
    val alu = aluPlugin.alu

    val branchInstructionActive = False

    def default_branch_type_actions(): Unit = {

      branchInstructionActive := True

      alu.io.cmd.control := branchUnit.io.aluCtrl

      core.aluSrcA := branchUnit.io.aluSrcA
      core.aluSrcB := branchUnit.io.aluSrcB

      // TODO
      when(core.illegalRS1Addr | core.illegalRS2Addr) {
        core.raiseIllegalInstructionException()
      }
    }

    when(branchInstructionActive) {
      branchUnit.io.cmd.valid := True
      branchUnit.io.rsp.ready := True

      when(branchUnit.io.rsp.fire) {
        when(branchUnit.io.rsp.branchTaken) {
          core.pc_next := branchUnit.io.rsp.branchAddr
        } otherwise {
          // Branch not taken.
        }
        core.commitInstruction()
      }
    }

    val dec = core.getDecoderService()
    import Instructions._
    val ctrl = List(
      BEQ -> BranchUnitCtrl.BEQ,
      BNE -> BranchUnitCtrl.BNE,
      BLT -> BranchUnitCtrl.BLT,
      BLTU -> BranchUnitCtrl.BLTU,
      BGE -> BranchUnitCtrl.BGE,
      BGEU -> BranchUnitCtrl.BGEU
    )

    for ((instr, branchUnitCtrl) <- ctrl) {
      dec.registerInstruction(
        instr,
        core => {
          default_branch_type_actions()
          branchUnit.io.cmd.control := branchUnitCtrl
        }
      )
    }

    if (withFormalChecks) {
      // Formal check
      formal {
        val a = core.RS1
        val b = core.RS2
        val branchTaken = branchUnit.io.rsp.branchTaken

        when(!initstate()
          && !core.clockDomain.isResetActive
          && branchUnit.io.rsp.fire
          && !core.exception_invalid_instruction
        ) {
          switch(branchUnit.io.cmd.control) {
            import BranchUnitCtrl._
            is(BEQ) {
              assert(branchTaken === (a === b))
            }
            is(BNE) {
              assert(branchTaken === (a =/= b))
            }
            is(BLT) {
              assert(branchTaken === (a.asSInt < b.asSInt))
            }
            is(BLTU) {
              assert(branchTaken === (a.asUInt < b.asUInt))
            }
            is(BGE) {
              assert(branchTaken === !(a.asSInt < b.asSInt))
            }
            is(BGEU) {
              assert(branchTaken === !(a.asUInt < b.asUInt))
            }
          }
        }
      }
    }
  }
}


class SingleCycleBranchPlugin(withFormalChecks: Boolean = true,
                              XLEN: BitCount = 32 bits) extends Plugin {

  var branchUnit: SingleCycleBranchUnit = null

  override def setup(core: Attocore): Unit = {
    branchUnit = new SingleCycleBranchUnit()
  }

  override def build(core: Attocore): Unit = {

    branchUnit.io.cmd.valid := False
    branchUnit.io.rsp.ready := False
    branchUnit.io.cmd.aluRsp.aEqualsB := core.aluResultEquals
    branchUnit.io.cmd.aluRsp.aLessThanB := core.aluResultLess
    branchUnit.io.cmd.aluRsp.result := core.aluResult
    branchUnit.io.cmd.control := BranchUnitCtrl.NO_BRANCH

    branchUnit.io.imm_b := core.instructionSplitter.io.imm_sb_type.asSInt.resize(XLEN).asUInt
    branchUnit.io.pc := core.pc

    val aluPlugins = core.findPlugins(classOf[IntAluPlugin])
    assert(aluPlugins.length == 1, s"Expected one ALU plugin. Found ${aluPlugins.length}")
    val aluPlugin = aluPlugins(0)
    val alu = aluPlugin.alu

    val branchInstructionActive = False

    def default_branch_type_actions(): Unit = {

      branchInstructionActive := True

      alu.io.cmd.control := branchUnit.io.aluCtrl

      core.aluSrcA := branchUnit.io.aluSrcA
      core.aluSrcB := branchUnit.io.aluSrcB

      // TODO
      when(core.illegalRS1Addr | core.illegalRS2Addr) {
        core.raiseIllegalInstructionException()
      }
    }

    when(branchInstructionActive) {
      branchUnit.io.cmd.valid := True
      branchUnit.io.rsp.ready := True

      when(branchUnit.io.rsp.fire) {
        when(branchUnit.io.rsp.branchTaken) {
          core.pc_next := branchUnit.io.rsp.branchAddr
        } otherwise {
          // Branch not taken.
        }
        core.commitInstruction()
      }
    }

    val dec = core.getDecoderService()
    import Instructions._
    val ctrl = List(
      BEQ -> BranchUnitCtrl.BEQ,
      BNE -> BranchUnitCtrl.BNE,
      BLT -> BranchUnitCtrl.BLT,
      BLTU -> BranchUnitCtrl.BLTU,
      BGE -> BranchUnitCtrl.BGE,
      BGEU -> BranchUnitCtrl.BGEU
    )

    for ((instr, branchUnitCtrl) <- ctrl) {
      dec.registerInstruction(
        instr,
        core => {
          default_branch_type_actions()
          branchUnit.io.cmd.control := branchUnitCtrl
        }
      )
    }

    if (withFormalChecks) {
      // Formal check
      formal {
        val a = core.RS1
        val b = core.RS2
        val branchTaken = branchUnit.io.rsp.branchTaken

        when(!initstate()
          && !core.clockDomain.isResetActive
          && branchUnit.io.rsp.fire
          && !core.exception_invalid_instruction
        ) {
          switch(branchUnit.io.cmd.control) {
            import BranchUnitCtrl._
            is(BEQ) {
              assert(branchTaken === (a === b))
            }
            is(BNE) {
              assert(branchTaken === (a =/= b))
            }
            is(BLT) {
              assert(branchTaken === (a.asSInt < b.asSInt))
            }
            is(BLTU) {
              assert(branchTaken === (a.asUInt < b.asUInt))
            }
            is(BGE) {
              assert(branchTaken === !(a.asSInt < b.asSInt))
            }
            is(BGEU) {
              assert(branchTaken === !(a.asUInt < b.asUInt))
            }
          }
        }
      }
    }
  }
}
