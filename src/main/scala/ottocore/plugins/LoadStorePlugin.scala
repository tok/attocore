package ottocore.plugins

import ottocore._
import spinal.core._
import spinal.lib._
import spinal.lib.fsm._

case class MemDataRequest(XLEN: BitCount = 32 bits) extends Bundle {
  val addr = UInt(XLEN)
  val wdata = Bits(XLEN)
  val wstrb = Bits(XLEN.value / 8 bits)
}

object LSUCtrl extends SpinalEnum {
  val NOP, LW, LH, LHU, LB, LBU, SW, SH, SB = newElement() // TODO: Get rid of NOP.
}

case class LSUCmd(XLEN: BitCount = 32 bits) extends Bundle {
  val addr = UInt(XLEN)
  val wdata = Bits(XLEN)
  val control = LSUCtrl()
}

case class LSURsp(XLEN: BitCount = 32 bits) extends Bundle {
  val rdata = Bits(XLEN)
  val misaligned_exception = Bool()
}


class LoadStoreUnit(XLEN: BitCount = 32 bits) extends Component {
  val io = new Bundle {

    val cmd = slave Stream LSUCmd(XLEN)
    val rsp = master Stream LSURsp(XLEN)

    // Interface to memory.
    val mem_req = master Stream MemDataRequest(XLEN)
    val mem_rsp = slave Stream Bits(XLEN)
  }


  // Wire up request interface.
  val addr = io.cmd.addr
  io.mem_req.valid := io.cmd.valid
  io.cmd.ready := io.mem_req.ready
  io.mem_req.addr := addr
  val wstrb = B(0, XLEN.value / 8 bits)
  io.mem_req.wstrb := wstrb

  // Wire up response interface.
  io.mem_rsp.ready := io.rsp.ready
  io.rsp.valid := io.mem_rsp.valid

  val rdata_w = io.mem_rsp.payload

  // Select half-word.
  // RISCV is Little Endian. Respect that for the addressing.
  val chunks_h = Vec(rdata_w.asBools.grouped(16).map(_.asBits()))
  val offset_h = addr(1).asUInt
  val rdata_h = chunks_h(offset_h)

  // Select byte.
  val chunks_b = Vec(rdata_w.asBools.grouped(8).map(_.asBits()))
  val offset_b = addr(1 downto 0).asBits.asUInt
  val rdata_b = chunks_b(offset_b)


  val rdata = Bits(XLEN)
  rdata := rdata_w // TODO: Don't care as default.
  io.rsp.rdata := rdata

  // Exceptions
  val exception = False
  io.rsp.misaligned_exception := exception

  // Write data for a full word write operation (SW).
  val wdata_w = io.cmd.wdata

  // Shift the write data based on the address offset.
  val wdata_h = Mux(addr(1), wdata_w |<< 16, wdata_w)
  val wdata_b = Mux(addr(0), wdata_h |<< 8, wdata_h)

  // Always assign the shifted data.
  // This can be done because an error will be raisen if
  // the data is shifted by an invalid offset.
  io.mem_req.wdata := wdata_b

  switch(io.cmd.control) {
    import LSUCtrl._
    is(NOP) {
      io.rsp.valid := False
      io.mem_req.valid := False
      io.mem_rsp.ready := False
    }
    is(LW) {
      rdata := rdata_w
      exception := addr(0 to 1) =/= 0
    }
    is(LH) {
      rdata := rdata_h.asSInt.resize(XLEN).asBits
      exception := addr(0 to 0) =/= 0
    }
    is(LHU) {
      rdata := rdata_h.asUInt.resize(XLEN).asBits
      exception := addr(0 to 0) =/= 0
    }
    is(LB) {
      rdata := rdata_b.asSInt.resize(XLEN).asBits
    }
    is(LBU) {
      rdata := rdata_b.asUInt.resize(XLEN).asBits
    }
    is(SW) {
      wstrb := B"1111"
      exception := addr(0 to 1) =/= 0
    }
    is(SH) {
      wstrb := addr(1).asUInt.mux(
        0 -> B"0011",
        1 -> B"1100"
      )
      exception := addr(0 to 0) =/= 0
    }
    is(SB) {
      wstrb := addr(1 downto 0).mux(
        0 -> B"0001",
        1 -> B"0010",
        2 -> B"0100",
        3 -> B"1000"
      )
    }
  }
}

class LoadStorePlugin extends Plugin {
  var lsu: LoadStoreUnit = null

  override def setup(core: Attocore): Unit = {
    lsu = new LoadStoreUnit()
  }

  override def build(core: Attocore): Unit = {
    // Wire up request memory interface.
    core.io.d_req_valid_o := lsu.io.mem_req.valid
    core.io.d_req_addr_o := lsu.io.mem_req.addr
    core.io.d_req_wstrb_o := lsu.io.mem_req.wstrb
    core.io.d_req_wdata_o := lsu.io.mem_req.wdata
    lsu.io.mem_req.ready := core.io.d_req_ready_i

    // Wire up response memory interface.
    lsu.io.mem_rsp.valid := core.io.d_rsp_valid_i
    core.io.d_rsp_ready_o := lsu.io.mem_rsp.ready
    lsu.io.mem_rsp.payload := core.io.d_rsp_rdata_i

    core.lsuResult := lsu.io.rsp.rdata

    val lsuCtrl = LSUCtrl.NOP()
    lsu.io.cmd.control := lsuCtrl
    lsu.io.cmd.addr := core.aluResult.asUInt
    lsu.io.cmd.wdata := core.RS2

    lsu.io.cmd.valid := False
    lsu.io.rsp.ready := False

    val dec = core.getDecoderService()

    val lsuLoadActive = False
    val lsuStoreActive = False

    val fullRequestResponseHandshake = true

    if (fullRequestResponseHandshake) {
      val fsm = new StateMachine {
        val stateInit: State = new State with EntryPoint {
          whenIsActive {
            when(lsuLoadActive | lsuStoreActive) {
              when(!core.any_exception) {
                goto(stateRequest)
              }
            }
          }
        }

        val stateRequest = new State {
          whenIsActive {
            lsu.io.cmd.valid := True
            when(lsu.io.cmd.fire) {
              ////               Dont wait for response when storing data.
              //              when(lsuLoadActive) {
              //                goto(stateResponse)
              //              } otherwise {
              //                core.commitInstruction()
              //                goto(stateInit)
              //              }
              goto(stateResponse)
            }
          }
        }

        val stateResponse = new State {
          whenIsActive {
            lsu.io.rsp.ready := True
            when(lsu.io.rsp.fire) {
              when(lsuLoadActive) {
                core.writeBackRegisterEnable := True
              }
              core.exception_misaligned_load_store := lsu.io.rsp.misaligned_exception

              core.commitInstruction()
              goto(stateInit)
            }
          }
        }
      }
    } else {

      // Independent load/store handshakes. Is this what the ottocore specification wants???

      val waitForRequestReady = RegInit(False)
      val waitForResponseValid = RegInit(False)

      // Request
      when(waitForRequestReady) {
        lsu.io.cmd.valid := True
        when(lsu.io.cmd.fire) {
          waitForRequestReady := False
          when(lsuStoreActive) {
            core.exception_misaligned_load_store := lsu.io.rsp.misaligned_exception
            core.commitInstruction()
          }
        }
      } otherwise {
        when(lsuStoreActive) {
          waitForRequestReady := True
          waitForResponseValid := True
        }
      }

      // Response
      when(waitForResponseValid) {
        lsu.io.rsp.ready := True
        when(lsu.io.rsp.fire) {
          waitForResponseValid := False

          when(lsuLoadActive) {
            core.writeBackRegisterEnable := True
          }
          core.exception_misaligned_load_store := lsu.io.rsp.misaligned_exception

          core.commitInstruction()
        }
      } otherwise {
        when(lsuLoadActive) {
          waitForRequestReady := True
          waitForResponseValid := True
        }
      }

    }

    //
    //    when(lsuStoreActive) {
    //      lsu.io.cmd.valid := True
    //      when(lsu.io.cmd.fire) {
    //        core.commitInstruction()
    //      }
    //    }
    //
    //    when(lsuLoadActive) {
    //      lsu.io.rsp.ready := True
    //
    //      when(lsu.io.rsp.fire) {
    //        core.writeBackRegisterEnable := True
    //        core.exception_misaligned_load_store := lsu.io.rsp.misaligned_exception
    //        core.commitInstruction()
    //      }
    //    }

    def default_load_type_actions(): Unit = {
      lsuLoadActive := True
      core.aluSrcA := AluSrcA.RS1

      core.aluSrcB := AluSrcB.IMM_I_S // TODO: sign extend immediate or not?
      core.aluCtrl := AluCtrl.ADD

      // Handle illegal register addresses.
      when(core.illegalRS1Addr | core.illegalRDAddr) {
        core.raiseIllegalInstructionException()
      }

      // Assign RD from memory.
      // Store result of LSU into register.
      core.rdWData := core.lsuResult

    }

    def default_store_type_actions(): Unit = {
      lsuStoreActive := True

      // Use ALU to compute memory address ( = RS1 + IMM )
      core.aluSrcA := AluSrcA.RS1
      // TODO: Sign extend or not?
      core.aluSrcB := AluSrcB.IMM_S_S
      core.aluCtrl := AluCtrl.ADD

      // Handle illegal register addresses.
      when(core.illegalRS1Addr | core.illegalRS2Addr) {
        core.raiseIllegalInstructionException()
      }
    }

    import Instructions._
    dec.registerInstruction(
      LB,
      core => {
        default_load_type_actions()
        lsuCtrl := LSUCtrl.LB
      }
    )
    dec.registerInstruction(
      LBU,
      core => {
        default_load_type_actions()
        lsuCtrl := LSUCtrl.LBU
      }
    )
    dec.registerInstruction(
      LH,
      core => {
        default_load_type_actions()
        lsuCtrl := LSUCtrl.LH
      }
    )
    dec.registerInstruction(
      LHU,
      core => {
        default_load_type_actions()
        lsuCtrl := LSUCtrl.LHU
      }
    )
    dec.registerInstruction(
      LW,
      core => {
        default_load_type_actions()
        lsuCtrl := LSUCtrl.LW
      }
    )
    dec.registerInstruction(
      SB,
      core => {
        default_store_type_actions()
        lsuCtrl := LSUCtrl.SB
      }
    )
    dec.registerInstruction(
      SH,
      core => {
        default_store_type_actions()
        lsuCtrl := LSUCtrl.SH
      }
    )
    dec.registerInstruction(
      SW,
      core => {
        default_store_type_actions()
        lsuCtrl := LSUCtrl.SW
      }
    )
  }
}