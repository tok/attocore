package ottocore.plugins

import ottocore._
import spinal.core._

class InstructionDecoderPlugin extends Plugin with DecoderService {

  val XLEN = 32 bits // register size
  val NUM_REGISTERS = 16
  val REG_ADDR_LEN = log2Up(NUM_REGISTERS) bits

  var instructions: Array[(MaskedLiteral, Attocore => Unit)] = Array()

  def registerInstruction(instruction: MaskedLiteral, action: Attocore => Unit): Unit = {
    instructions ++= List((instruction, action))
  }

  override def setup(core: Attocore): Unit = {

    core.aluCtrl := AluCtrl.NOP

    // By default there is no interrupt.
    core.waitForInterrupt := False

    // Default sources for ALU operands.
    core.aluSrcA := AluSrcA.RS1
    core.aluSrcB := AluSrcB.RS2

    // Default source for RD.
    core.rdWData := 0 // TODO: 'don't-care' as default could save some area


  }

  override def post(core: Attocore): Unit = {

    val instructionDecoder = new Area {

      // Split instruction into useful signals.
      // (Plain rewiring, no logic involved)
      val splitter = new InstructionSplitter
      splitter.io.instruction := core.currentInstruction

      // Register addresses.
      // Addresses are cropped to the required range as RV32E does only use 16 registers.
      val registerAddrRange = (0 until REG_ADDR_LEN.value)
      val rs1_addr = splitter.io.rs1(registerAddrRange).asUInt
      val rs2_addr = splitter.io.rs2(registerAddrRange).asUInt
      val rd_addr = splitter.io.rd(registerAddrRange).asUInt

      // Default addresses for RS1, RS1
      core.rs1_addr := rs1_addr
      core.rs2_addr := rs2_addr
      core.rd_addr := rd_addr


      // On an illegal register address a 'illegal instruction' error must be thrown.
      assert(NUM_REGISTERS == 16, "Currently only RV32E supported.")
      core.illegalRS1Addr := splitter.io.rs1(4)
      core.illegalRS2Addr := splitter.io.rs2(4)
      core.illegalRDAddr := splitter.io.rd(4)


      // Decode instructions.
      // Derive control signals from the instruction.
      switch(core.currentInstruction) {

        // Create an `is` switch case for all registered instruction.
        // If the current instruction matches the registered instruction then the registered actions will be activated.
        for ((instruction, action) <- instructions) {
          println(s"Decode: $instruction")
          is(instruction) {
            when(core.instructionPresent) {
              action(core)
            }
          }
        }

        default {
          // Illegal or not implemented instruction.
          core.raiseIllegalInstructionException()
        }
      }
    }
  }
}
