package ottocore.plugins

import ottocore._
import spinal.core._

/**
 * Implements the register file for the core.
 *
 * @param numRegisters       Number of registers including r0. Must be 16 or 32.
 * @param XLEN               Bit width of registers.
 * @param numFormalRegisters Number of active registers for formal checks (including r0).
 * @param resetToZero        true: Register file is set to all zeros on reset, false: register file data is undefined.
 *                           Allowing the registers to have undefined state might save some area because registers without reset/preload
 *                           might be used.
 */
class RegisterFilePlugin(numRegisters: Int = 16,
                         XLEN: BitCount = 32 bits,
                         numFormalRegisters: Int = 3,
                         resetToZero: Boolean = true
                        ) extends Plugin {

  override def setup(core: Attocore): Unit = {
    // === Register file ===
    val registerFile = new RegisterFile(
      numRegisters = numRegisters,
      XLEN = XLEN,
      numFormalRegisters = numFormalRegisters
    )
    core.RS1 := registerFile.io.r_data1
    core.RS2 := registerFile.io.r_data2

    // Wiring up register file.
    registerFile.io.r_addr1 := core.rs1_addr
    registerFile.io.r_addr2 := core.rs2_addr
    registerFile.io.w_addr := core.rd_addr
    registerFile.io.w_valid := core.writeBackRegisterEnable
    registerFile.io.w_data := core.rdWData
  }

}
