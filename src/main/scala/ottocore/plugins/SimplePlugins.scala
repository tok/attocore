package ottocore.plugins

import spinal.core._
import ottocore._

/**
 * Collection of simple and dummy plugins.
 */

/**
 * Implements WFI instruction.
 */
class WFIPlugin extends Plugin {
  override def setup(core: Attocore): Unit = {

  }

  override def build(core: Attocore): Unit = {
    core.getDecoderService().registerInstruction(
      Instructions.WFI,
      core => {
        core.waitForInterrupt := True
        core.commitInstruction()
      }
    )
  }
}

/**
 * Implements given instructions as NOPs.
 */
class DummyNOPs(instructions: List[MaskedLiteral]) extends Plugin {
  override def setup(core: Attocore): Unit = {

  }

  override def build(core: Attocore): Unit = {

    for (op <- instructions) {
      core.getDecoderService().registerInstruction(
        op,
        core => {
          core.commitInstruction()
        }
      )
    }
  }
}
